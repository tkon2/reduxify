/* global Prism */

// eslint-disable-next-line no-unused-vars
class Content {
    constructor(element, options) {
        this.text = '';
        this.element = element;
        this.depth = 0;
        this.options = options;
        this.code_section_count = 0;
    }

    checkDepth(level, new_depth) {
        if (this.depth < level) {
            throw new Error(`Not deep enough, you're missing beginDocument, beginSection, or beginCode. Level was ${  level  } and depth was ${  this.depth}`);
        } else if (this.depth > level) {
            throw new Error(`Too deep, you're missing endCode, or endSection. Level was ${  level  } and depth was ${  this.depth}`);
        } else {
            this.depth = new_depth;
        }
    }

    beginDocument() {
        this.checkDepth(0, 1);
        this.text += '<div class="main-wrapper">';
        return this;
    }

    beginSection(title) {
        this.checkDepth(1, 2);
        this.text += '<div class="section-wrapper">';
        this.text += '<h3>';
        this.text += title;
        this.text += '</h3>';
        return this;
    }

    beginCode(tab_depth) {
        this.checkDepth(2, 3);
        this.code_section_count += 1;
        this.indent = this.options.indent ? ' '.repeat(tab_depth * 4) : '';
        this.text += '<div class="code-section-wrapper">';
        this.text += '<pre>';
        this.text += `<code class="language-javascript" id="code-section-${this.code_section_count}">`;
        if (this.options.leading) {this.text+='\n';}
        this.first_line = true;
        return this;
    }

    line(text) {
        this.checkDepth(3, 3);
        if(this.first_line) {
            this.first_line = false;
        } else {
            this.text += '\n';
        }
        this.text += `${this.indent}${text}`;
        return this;
    }

    endCode() {
        this.checkDepth(3, 2);
        if (this.options.trailing) { this.text += '\n'; }
        this.text += '</code>';
        this.text += '</pre>';
        this.text += `<button class="copy-button" data-clipboard-target="#code-section-${this.code_section_count}">Click to copy</button >`;
        this.text += '</div>';
        return this;
    }

    endSection() {
        this.checkDepth(2, 1);
        this.text += '</div>';
        return this;
    }

    export() {
        this.text += '</div>';
        if (this.element) {
            this.element.innerHTML = this.text;
            Prism.highlightAll();
        }
        return this.text;
    }

}

// eslint-disable-next-line no-unused-vars
function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

// eslint-disable-next-line no-unused-vars
function tokenize(text) {
    let tokens = [];
    if (text.includes('_')) {
        tokens = text.split('_');
    } else {
        tokens = text.split(/(?=[A-Z])/);
    }
    tokens = tokens.filter(t => (t !== ''));
    return tokens.map(t => t.toLowerCase());
}
