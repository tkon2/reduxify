/* global Content document capitalizeFirstLetter tokenize ClipboardJS */

const redraw = () => {
    const leading_newline = document.getElementById('leading-newline-checkbox').checked;
    const trailing_newline = document.getElementById('trailing-newline-checkbox').checked;
    const indentation = document.getElementById('indentation-checkbox').checked;
    const input_element = document.getElementById('main-input');
    const input_text = input_element.value || 'REDUX_EXAMPLE';
    const normalized_text = input_text.replace(/-/g, '_').replace(/[^0-9a-zA-Z_]/g, '').replace(/__/g, '_');
    if (normalized_text !== 'REDUX_EXAMPLE') {
        input_element.value = normalized_text;
    }
    const tokens = tokenize(normalized_text);

    const lower = tokens.reduce((c, t, i) => c += i === 0 ? t : capitalizeFirstLetter(t), '');
    const upper = tokens.reduce((c, t, i) => c += (i === 0 ? '' : '_') + t.toUpperCase(), '');

    const content = new Content(document.getElementById('root'), {
        leading: leading_newline,
        trailing: trailing_newline,
        indent: indentation,
    });
    content.beginDocument()
        .beginSection('Actions')
        .beginCode(1)
        .line(`${upper}_REQUEST: null,`)
        .line(`${upper}_SUCCESS: null,`)
        .line(`${upper}_FAILURE: null,`)
        .endCode()
        .beginCode(0)
        .line('export const {')
        .line(`    ${lower}Request,`)
        .line(`    ${lower}Success,`)
        .line(`    ${lower}Failure,`)
        .line('} = createActions (')
        .line(`    constants.${upper}_REQUEST,`)
        .line(`    constants.${upper}_SUCCESS,`)
        .line(`    constants.${upper}_FAILURE,`)
        .line(');')
        .endCode()
        .endSection()
        .beginSection('Reducer')
        .beginCode(1)
        .line(`${upper}_REQUEST,`)
        .line(`${upper}_SUCCESS,`)
        .line(`${upper}_FAILURE,`)
        .endCode()
        .beginCode(1)
        .line(`case ${upper}_REQUEST:`)
        .line('    return state.set(\'isFetching\', true)')
        .line('        .set(\'error\', null);')
        .endCode()
        .beginCode(1)
        .line(`case ${upper}_SUCCESS:`)
        .line('    return state.set(\'isFetching\', false);')
        .endCode()
        .beginCode(1)
        .line(`case ${upper}_FAILURE:`)
        .line('    return state.set(\'isFetching\', false)')
        .line('        .set(\'error\', payload);')
        .endCode()
        .endSection()
        .beginSection('Saga')
        .beginCode(2)
        .line(`${lower}Success,`)
        .line(`${lower}Failure,`)
        .endCode()
        .beginCode(2)
        .line(`${upper}_REQUEST,`)
        .endCode()
        .beginCode(1)
        .line(`function* ${lower}() {`)
        .line('    try {')
        .line('        const response = yield call(fetchApiAuth, {')
        .line('            method: \'GET\',')
        .line('            url: \'get-profile\',')
        .line('        });')
        .line('')
        .line(`        yield put(${lower}Success(response));`)
        .line('    } catch (e) {')
        .line('        message.error(e.response ? e.response.data.message : e);')
        .line(`        yield put(${lower}Failure(e.response ? e.response.data.message : e));`)
        .line('    }')
        .line('}')
        .endCode()
        .beginCode(1)
        .line(`takeEvery(${upper}_REQUEST, ${lower}),`)
        .endCode()
        .endSection()
        .export();
};

document.addEventListener('DOMContentLoaded', () => {
    document.getElementById('main-input').onchange = redraw;
    document.getElementById('main-input').onkeyup = redraw;
    document.getElementById('leading-newline-checkbox').onchange = redraw;
    document.getElementById('trailing-newline-checkbox').onchange = redraw;
    document.getElementById('indentation-checkbox').onchange = redraw;

    // Initialize clipboard.js
    new ClipboardJS('.copy-button');
    redraw();
});
